﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Data;
using ConsoleApplication1.ShipExec;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine(thisApiCall(1,0,0));
            //Console.WriteLine(thisApiCall(1,1,0));       
             
            Console.WriteLine(thisApiCall(2,0,0));
            //Console.WriteLine(thisApiCall(2,1,0));
             
            Console.WriteLine(thisApiCall(3,0,0));
            //Console.WriteLine(thisApiCall(3,1,0));
           
            Console.WriteLine(thisApiCall(4,0,0));
            //Console.WriteLine(thisApiCall(4,1,0));
            
            Console.WriteLine(thisApiCall(5,0,0)); 
            //Console.WriteLine(thisApiCall(5,1,0));
            
            Console.WriteLine(thisApiCall(6,0,0));
            //Console.WriteLine(thisApiCall(6,1,0));
                       
            Console.WriteLine(thisApiCall(7,0,0));
            //Console.WriteLine(thisApiCall(7,1,0));
            
            Console.WriteLine(thisApiCall(8,0,1));
            //Console.WriteLine(thisApiCall(8,1,1));
            //Console.WriteLine(thisApiCall(8,1,0));
            
            Console.WriteLine("\n\n");

            Console.Clear();
            Console.WriteLine("API Test Complete");
            Console.WriteLine("Press a key to exit");
            
            Console.ReadKey();

        }
  
       static string thisApiCall(int whichCall, int rateType, int COD)
        {
    
            ShipmentRequest mySR = new ShipmentRequest();
            ShipmentRequest myCR = new ShipmentRequest();

            NameAddress na = new NameAddress(); 

            decimal weight;
            string symbol;

            switch (whichCall)
            {
                // Local Address
                case 1:
                    na.company = "Dookie LLC";
                    na.address1 = "6121 E Eastland St";
                    na.city = "Tucson";
                    na.state_province = "AZ";
                    na.postal_code = "85711";
                    na.country_symbol = "UNITED_STATES";
                    weight = 4;
                    symbol = "LB";
                    break;

                // LA Address - P.O. Box
                case 2:
                    na.company = "Earth Class";
                    na.address1 = "PO Box 515381";
                    na.city = "Los Angeles";
                    na.state_province = "CA";
                    na.postal_code = "90051-6681";
                    na.country_symbol = "UNITED_STATES";
                    weight = 1;
                    symbol = "LB";
                    break;


                // Domestic Address - South Bend Indiana
                case 3:
                    na.company = "EC Internet";
                    na.address1 = "4023 W Sample St";
                    na.city = "South Bend";
                    na.state_province = "IN";
                    na.postal_code = "46619";
                    na.country_symbol = "UNITED_STATES";
                    weight = 6;
                    symbol = "OZ";
                    break;

                // Virgin Islands
                case 4:
                    na.company = "Test Inc..";
                    na.address1 = "7100 ESTATE BELLEVUE";
                    na.city = "St Thomas";
                    na.state_province = "VI";
                    na.postal_code = "00802";
                    na.country_symbol = "US_VIRGIN_ISLANDS";
                    weight = 18;
                    symbol = "LB";
                    break;

                // Puerto Rico
                case 5:
                    na.company = "A Comapny Inc.";
                    na.address1 = "OLD SAN JUAN STA";
                    na.address2 = "PO BOX 9021190";
                    na.city = "SAN JUAN";
                    na.state_province = "PR";
                    na.postal_code = "00902";
                    na.country_symbol = "PUERTO_RICO";
                    weight = 31;
                    symbol = "LB";
                    break;

                // Canada
                case 6:
                    na.company = "Oxford University";
                    na.address1 = "22 Knightbridge Rd";
                    na.city = "Amprior";
                    na.state_province = "Ontario";
                    na.postal_code = "K2S 0Z7";
                    na.country_symbol = "CANADA";
                    weight = 8;
                    symbol = "LB";
                    break;

                // Great Britian
                case 7:
                    na.company = "Winston Churchill";
                    na.address1 = "The Old Schools";
                    na.city = "Trinity Ln";
                    na.state_province = "Cambridge";
                    na.postal_code = "CB2 1TN";
                    na.country_symbol = "UNITED_KINGDOM";
                    weight = 4;
                    symbol = "LB";
                    break;

                // FUTURE -- Within State of Arizona -- C.O.D. enabled
                case 8:
                    na.company = "PB";
                    na.address1 = "5407 E Boulder Run Dr";
                    na.city = "Flagstaff";
                    na.state_province = "AZ";
                    na.postal_code = "86004";
                    na.country_symbol = "UNITED_STATES";
                    weight = Convert.ToDecimal("4.5");
                    symbol = "LB";
                    break;

                // FUTURE -- Open
                case 9:
                    na.company = "Oxford University";
                    na.address1 = "22 Knightbridge Rd";
                    na.city = "Amprior";
                    na.state_province = "Ontario";
                    na.postal_code = "K2S 0Z7";
                    na.country_symbol = "CANADA";
                    weight = 8;
                    symbol = "OZ";
                    break;


                // FUTURE --Open
                case 10:
                    na.company = "Oxford University";
                    na.address1 = "22 Knightbridge Rd";
                    na.city = "Amprior";
                    na.state_province = "Ontario";
                    na.postal_code = "K2S 0Z7";
                    na.country_symbol = "CANADA";
                    weight = 8;
                    symbol = "OZ";
                    break;


                default:
                    na.company = "RageMonkey";
                    na.address1 = "6121 E Eastland St";
                    na.city = "Tucson";
                    na.state_province = "AZ";
                    na.postal_code = "85711";
                    na.country_symbol = "UNITED_STATES";
                    weight = 2;
                    symbol = "LB";  // or "OZ"
                    break;
            }


            mySR.def_attr = new Package();
            mySR.def_attr.consignee = na;
            mySR.def_attr.shipper = "HSPP"; // "HSPP"
            mySR.def_attr.subcategory = "CONNECTSHIP_UPS.UPS.GND";
            mySR.def_attr.terms = "SHIPPER";

            myCR.def_attr = new Package();
            myCR.def_attr.consignee = na;
            myCR.def_attr.shipper = "HSPP";
            myCR.def_attr.subcategory = "CONNECTSHIP_UPS.UPS.GND";
            myCR.def_attr.terms = "SHIPPER";

            DateTime dt = new DateTime();
            dt = DateTime.Now;
            
            mySR.def_attr.shipdate = dt;
            myCR.def_attr.shipdate = dt;

            PackageRequest pr = new PackageRequest();
            pr.consignee_reference = "Test";
            pr.packaging = "CUSTOM";
            pr.shipper_reference = "Test";
            pr.weight = weight;
            pr.weight_unit = symbol;
            pr.declared_value_customs =  Convert.ToDecimal("35.00");

            PackageRequest cr = new PackageRequest();
            cr.consignee_reference = "Test";
            cr.packaging = "CUSTOM";
            cr.shipper_reference = "Test";
            cr.weight = weight;
            cr.weight_unit = symbol;
            cr.declared_value_customs = Convert.ToDecimal("35.00");

            if (COD == 1)
            {
                pr.cod_amount = Convert.ToDecimal("100");
                pr.cod_method = 1;
                pr.cod_payment_method = 2;

                cr.cod_amount = Convert.ToDecimal("100");
                cr.cod_method = 1;
                cr.cod_payment_method = 2;
            }

           /*
                cpmCash	Payment in cash. 	1
                cpmCashiersCheck	Payment by cashiers check. 	2
                cpmBusinessCheck	Payment by business check. 	4
                cpmMoneyOrder	Payment by money order. 	8
                cpmPersonalCheck	Payment by personal check. 	16
                cpmPostDatedBusinessCheck	Payment by post-dated business check. 	32
                cpmPostDatedPersonalCheck	Payment by post-dated personal check. 	64
                cpmAny	Any payment method. 	127
           */


            //pr.shipper = "HSPP";

            
            mySR.packages = new PackageRequest[] { pr };
            myCR.packages = new PackageRequest[] { cr };


            SoxDictionaryItem[] params1;
            SoxDictionaryItem[] params2;
           

            params2 = new SoxDictionaryItem[]
            {
                new SoxDictionaryItem(){ key = "InProcessRating", value = "" },                                                
                new SoxDictionaryItem(){ key = "CustomerRate", value = "1"}
            };


            params1 = new SoxDictionaryItem[]
            {
                new SoxDictionaryItem(){ key = "InProcessRating", value = "" }
            };



//            ship_req.packages[0].user_data1 = new SoxDictionary();
            
           // cod amt supplied then add cod amount - get cod fee
           
            //ShipmentResponse sr;
           
            Console.Clear();
            Console.WriteLine("Testing API... \n\n");

            Console.WriteLine("-----------------------------------");
            Console.WriteLine("TEST CASE #" + whichCall + "\n");

            Console.WriteLine(pr.weight + " " + pr.weight_unit + " - Shipping to " + na.address1 + " " + na.address2 + " " + na.city + ", " + na.state_province + " " + na.postal_code);
            //Console.WriteLine("Shipping to " + na.address1 + " " + na.address2 + " " + na.city + ", " + na.state_province + " " + na.postal_code);
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("\n\n");

            string[] myServices = new string[] {
                "CONNECTSHIP_UPS.UPS.GND", 
                "CONNECTSHIP_UPS.UPS.3DA", 
                "CONNECTSHIP_UPS.UPS.2DA", 
                "CONNECTSHIP_UPS.UPS.2AM", 
                "CONNECTSHIP_UPS.UPS.NDA", 
                "CONNECTSHIP_UPS.UPS.NAM", 
                "CONNECTSHIP_UPS.UPS.EXP",
                "CONNECTSHIP_UPS.UPS.EXPSVR",
                "CONNECTSHIP_UPS.UPS.EPD", 
                "CONNECTSHIP_ENDICIA.USPS.FIRST", 
                "CONNECTSHIP_ENDICIA.USPS.PRIORITY", 
                "CONNECTSHIP_ENDICIA.USPS.I_FIRST", 
                "CONNECTSHIP_ENDICIA.USPS.I_PRIORITY" 
            };
            
            using (var ship1 = new IwcfShipClient())
            {

                  ShipmentResponse[] SRs = ship1.RateServices(mySR, myServices, 0, ref params1);
                  ShipmentResponse[] CRs = ship1.RateServices(myCR, myServices, 0, ref params2);

                  decimal[] cRates = new decimal[13];
                  string[] cServices = new string[13];
                  int place = 0;

                  Dictionary<String,Decimal> z = new Dictionary<String, Decimal>();

                  foreach (ShipmentResponse c in CRs)
                  {
                      z.Add(c.def_attr.service, c.def_attr.total);
                  }

                  foreach ( ShipmentResponse s in SRs ) 
                  {


                      if ((s.def_attr.total == -1) || (s.def_attr.total == 0))
                      {
                        //Console.WriteLine("\n\n");
                      }

                      else
                      {
                        Console.WriteLine(s.def_attr.service);
                        //Console.WriteLine("Status: " + s.def_attr.error_message);
                        //Console.WriteLine("Time in Transit: " + s.def_attr.time_in_transit);
                        Console.WriteLine("Days in Transit: " + s.def_attr.time_in_transit_days);
                        Console.WriteLine("Delivery Date: " + s.def_attr.arrive_date);
                        Console.WriteLine("Published Rate: $" + s.def_attr.total);

                        if (z[s.def_attr.service] != s.def_attr.total)
                        {
                            Console.WriteLine("Customer Rate : $" + z[s.def_attr.service]);
                            //Console.WriteLine("CS: " + cServices[place]);
                        }

                        Console.WriteLine("\n\n");

                      }

                  }

                //Console.WriteLine("\n\n");
                Console.WriteLine("Press a key to continue");
                Console.ReadKey();
            }


            return "ok";

        }
    
    }

 
}
